---
layout: post
title:  "Willkommen!"
date:   2018-11-02 11:22:48 +0100
categories: rursee-openair
---
Nach dem großartigen Spaß und den rundum zufriedenen Rückmeldungen bei unserem Rursee-Openair 2018: <i> Friede Freude Woffelskrach</i> freuen wir uns, dass das nächste Rursee-Openair schon einen festen Termin hat - und
jetzt auch eine Website!<br>
Hier werden in den nächsten Wochen und Monaten neue Infos zu den auftretenden Bands, dem Rahmenprogramm
und den Details stehen. Ihr könnt Euch jetzt schon anmelden für einen Übernachtungsplatz im Nabedi-Camp -
wartet nicht zu lange!
Bis bald, <br>
Euer Festivalkommittee :o)

<!-- more -->

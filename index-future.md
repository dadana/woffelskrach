---
layout: extra-with-logo
---

<article class="kasten">
  <h3>Das Rursee-Openair 2024:</h3>
  <h1 class="orange">FRIEDE - FREUDE - WOFFELSKRACH
  <time datetime="2024-06-21PT2D">21. - 23. Juni 2024</time></h1>
</article>
<br>
<article class="kasten kastexleft">

{% include aktuelles.md %}

</article>

<article class="kasten kastexleft">
## Allgemeines

<footer><time datetime="2024-02-14">14.02.24:</time></footer>
Herzlich Willkommen :o)
Auf diesen Seiten findet Ihr alle Infos über unser schönes Woffelskrach, das Rursee-Openair in Woffelsbach (Kreis Simmerath, Städteregion Aachen).
Das Festivalgelände ist ein Jugendgruppencamp und Campingplatz direkt am Rursee gelegen, wo die Festivaläste eine begrenzte Anzahl an <a href="location#schlaf">Übernachtungsorte buchen</a> können, sei es ganze Blockhütten oder Betten in Mehrbettzimmern, Zelt- oder Stellplätze.
Unser <a href="programm.html">Bühnenprogramm</a> soll wie immer ein wunderbarer Strauß an Können, Enthusiasmus und Kuriositäten werden.
Freitag und Samstag spielen <a href="bands.html">Bands</a>, die genauen Startzeiten werden nach und nach im Programm stehen.
Am Freitag Nachmittag starten wir mit dem wunderbaren <a href="programm.html#zirkus">Mittmachzirkus</a> für Groß und Klein vor der Bühne.
Freitag wird es ein bis zwei Undercover-Noch-No-Name-Darbietungen am Abend geben...uijuijui!

Das Festival vor allem durch den Getränkeverkauf und das Hutgeld.
Merke: Das in 2024 erstmals eingeführte Festivalticket ist hilfreich, aber langt natürlich nicht für ein zweitägiges Festival mit Live-Bands, allem Pipapo und ganz viel Liebe!
Also habt Euer Geld dabei und lasst Mitgebrachtes im Zelt/Zimmer. Hier weitere Infos zur <a href="location.html#eintritt">Finanzierung</a> und zu unseren <a href="sponsoren.html">Sponsoren</a>.

Der Getränke-Verkauf läuft während des gesamten Festivals entweder an den Ständen oder an der Rezeption. Essen gibt es dieses Jahr variationsreicher und schon ab Samstag Mittag - genauere Infos folgen.

Wirklich wichtige und sich rächende wenn ignorierte Infos zur <a href="location.html#reise">Anreise</a> hier.

Bleibt noch zu sagen: Wir freuen uns sehr auf Euch alle!

Liebe Grüße, Euer <a href="impressum.html" target="_blank">Festivalkommittee</a> :D
</article>

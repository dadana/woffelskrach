## Aktuelles

<footer><time datetime="2024-03-16">16.03.24</time></footer>
Die ersten [Bands](http://bands.html) sind gebucht!
Außerdem hier schon mal der Hinweis auf unser zweites(!!) Festival in diesem Jahr, dem **SPEKTAKEL AM SEE (OPENART)**, am 23. August.
Weitere Infos auf der [Natur bewegt Dich - Website](naturbewegtdich.de/aktuelles/107-spektakel-am-see).

<footer><time datetime="2024-03-16">16.03.24:</footer>
Sehr viele Schlafplätze sind schon vergeben. Anfragen für Übernachtsplätze bitte an
<a href="mailto:info@naturbewegtdich.desubject=Anfrage%20Woffelskrach%202024">info@naturbewegtdich.de</a>.

Diesmal findet das Woffelskrach NICHT am gleichen Tag wie das Woffelsbacher Drachenbootrennen statt. Wir haben also viel Zeit für das Bühnenprogramm am Freitag und Samstag. Das Drachenbootrennen findet statt dessen zwei Wochen vorher am Samstag 8. Juni statt und Nabedi nimmt natürlich wieder mit mindestens einer Mannschaft teil.
